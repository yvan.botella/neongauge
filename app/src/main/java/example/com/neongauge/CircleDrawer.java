package example.com.neongauge;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.View;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by yvan on 27/01/16.
 */
public class CircleDrawer extends View {

    float map(float x, float in_min, float in_max, float out_min, float out_max)
    {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    protected float _cx, _cy, _radius, _stroke;
    protected float _angle, _angle_min, _angle_max;
    protected int _direction;

    public CircleDrawer(Context context) {
        super(context);
        // TODO Auto-generated constructor stub
    }

    public CircleDrawer(Context context, AttributeSet attrs) {
        super(context, attrs);
        // TODO Auto-generated constructor stub
    }

    public CircleDrawer(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    protected void draw_arc_rounded(Canvas canvas, float radius, float stroke, float cx, float cy, float from, float to, Paint paint)
    {
        draw_circle(canvas, radius, stroke / 2, cx, cy, from, paint);
        draw_circle(canvas, radius, stroke / 2, cx, cy, to, paint);
        draw_arc(canvas, radius, stroke, cx, cy, from, to, paint);
    }

    protected void draw_arc(Canvas canvas, float radius, float stroke, float cx, float cy, float from, float to, Paint paint)
    {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(stroke);
        paint.setAntiAlias(true);

        final RectF oval = new RectF();

        oval.set(cx - radius,
                cy - radius,
                cx + radius,
                cy + radius);

        canvas.save();
        canvas.rotate(90, cx, cy);
        canvas.drawArc(oval, from, to - from, false, paint);
        canvas.restore();
    }

    protected void draw_circle(Canvas canvas, float radius, float size, float cx, float cy, float angle, Paint paint)
    {
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);

        float px = radius * (float)Math.cos(Math.toRadians(angle)) + cx;
        float py = radius * (float)Math.sin(Math.toRadians(angle)) + cy;

        canvas.save();
        canvas.rotate(90, cx, cy);
        if (radius == 0)
            canvas.drawCircle(cx, cy, size, paint);
        else
            canvas.drawCircle(px, py, size, paint);
        canvas.restore();
    }

    protected void draw_stroke(Canvas canvas, float radius, float size, float stroke, float cx, float cy, float angle, Paint paint)
    {
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(stroke);
        paint.setAntiAlias(true);

        float px = radius * (float)Math.cos(Math.toRadians(angle)) + cx;
        float py = radius * (float)Math.sin(Math.toRadians(angle)) + cy;

        canvas.save();
        canvas.rotate(90, cx, cy);
        if (radius == 0)
            canvas.drawCircle(cx, cy, size, paint);
        else
            canvas.drawCircle(px, py, size, paint);
        canvas.restore();
    }
}