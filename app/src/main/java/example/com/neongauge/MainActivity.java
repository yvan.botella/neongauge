package example.com.neongauge;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    MetroGauge gauge;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gauge = (MetroGauge)findViewById(R.id.metro_gauge);
    }

    @Override
    protected void onStart() {
        super.onStart();
        gauge.setValue(6890);
    }
}
