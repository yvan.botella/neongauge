package example.com.neongauge;

import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

public class MetroGauge extends FrameLayout {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private float _angle;

    private int _gauge_color_1;
    private int _gauge_color_2;
    private int _background_color;

    private int _text_unit_color;
    private float _text_unit_size;
    private String _text_unit_min;
    private String _text_unit_max;
    private int _text_value_color;
    private float _text_value_size;

    private int _value_min;
    private int _value_max;
    private int _value;

    private boolean _show_unit;
    private boolean _show_unit_min;
    private boolean _show_unit_max;

    private String _text_value;
    private boolean _show_needle;

    NeonGaugeView gauge;
    TextView textValue;

    public MetroGauge(Context context) {
        super(context);
        init();
    }

    public MetroGauge(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MetroGauge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public MetroGauge(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    private void init() {
        View view = View.inflate(getContext(), R.layout.fragment_metro_gauge, null);

        gauge = (NeonGaugeView)view.findViewById(R.id.gauge);
        textValue = (TextView)view.findViewById(R.id.text_value);
        TextView textValueUnit = (TextView)view.findViewById(R.id.text_value_unit);
        TextView textDate = (TextView)view.findViewById(R.id.text_date);
        TextView textUnitMin = (TextView)view.findViewById(R.id.text_unit_min);
        TextView textUnitMax = (TextView)view.findViewById(R.id.text_unit_max);

        Typeface latoBold = Typeface.createFromAsset(getContext().getAssets(), "Lato-Bold.ttf");
        Typeface latoReg = Typeface.createFromAsset(getContext().getAssets(), "Lato-Regular.ttf");
        textValue.setTypeface(latoBold);
        textValueUnit.setTypeface(latoBold);
        textDate.setTypeface(latoReg);
        textUnitMin.setTypeface(latoReg);
        textUnitMax.setTypeface(latoReg);

        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        addView(view);
    }

    public void setValue(int value)
    {
        animate_value_changed(value);
    }

    private void setXmlValues(TypedArray array) {


        // _radius = array.getFloat(R.styleable.Gauges_radius, 200);

        _background_color = array.getColor(R.styleable.Gauges_background_color, 0);

        _text_unit_color = array.getColor(R.styleable.Gauges_text_unit_color, 0);
        _text_unit_size = array.getDimension(R.styleable.Gauges_text_unit_size, 0);
        _text_unit_min = "0";
        _text_unit_max = "10000";
//        _text_unit_min = array.getText(R.styleable.Gauges_text_unit_min, "min");
//        _text_unit_max = array.getText(R.styleable.Gauges_text_unit_max, "max");
        _text_value_color = array.getColor(R.styleable.Gauges_text_value_color, 0);
        _text_value_size = array.getDimension(R.styleable.Gauges_text_value_size, 0);

            _gauge_color_1 = array.getColor(R.styleable.Gauges_gauge_color_1, 0);
            _gauge_color_2 = array.getColor(R.styleable.Gauges_gauge_color_2, 0);

        _value_min = array.getInt(R.styleable.Gauges_value_min, 0);
        _value_max = array.getInt(R.styleable.Gauges_value_max, 10000);

        _show_unit = array.getBoolean(R.styleable.Gauges_show_unit, true);
        _show_unit_min = array.getBoolean(R.styleable.Gauges_show_unit_min, true);
        _show_unit_max = array.getBoolean(R.styleable.Gauges_show_unit_max, true);

        _text_value = array.getString(R.styleable.Gauges_text_value);
        _show_needle = array.getBoolean(R.styleable.Gauges_show_needle, true);
    }

    private int _old_value = 0;
    ValueAnimator animator;
    public void animate_value_changed(final int value) {

        animator = ValueAnimator.ofInt(_old_value, value);
        animator.setDuration(2000);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                int animatedValue = (int) animation.getAnimatedValue();
                gauge.set_value(animatedValue);
                textValue.setText(String.valueOf(animatedValue));
            }
        });
        animator.start();
    }
}
