package example.com.neongauge;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.SweepGradient;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.FrameLayout;

/**
 * Created by yvan on 27/01/16.
 */
public class NeonGaugeView extends CircleDrawer {

    private int _gauge_color_1;
    private int _gauge_color_2;
    private int _background_color;

    private int _value_min;
    private int _value_max;
    private int _value;
    private int _old_value = 0;

    private boolean _show_needle;


    public NeonGaugeView(Context context) {
        this(context, null);
        // TODO Auto-generated constructor stub
    }

    public NeonGaugeView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NeonGaugeView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setXmlValues(context.getTheme().obtainStyledAttributes(attrs, R.styleable.Gauges, 0, 0));
        _value = 0;

        float width = (float)getWidth();
        float height = (float)getHeight();

        if (width > height)
            _radius = height * (float)0.45;
        else
            _radius = width * (float)0.45;

        setLayoutParams(new ViewGroup.LayoutParams((int)_radius/2, (int)_radius/2));
    }

    private int dpToPx(int dp)
    {
        float density = getContext().getResources().getDisplayMetrics().density;
        return Math.round((float)dp * density);
    }

    public int get_value()
    {
        return _value;
    }
    public void set_value(int value)
    {
        _value = value;
        if (_value < 0)
            _value = _value_min;
        else if (_value > _value_max)
            _value = _value_max;
        if (_old_value != _value)
            invalidate();
        _old_value = value;
    }

    public int get_value_min()
    {
        return _value_min;
    }
    public void set_value_min(int value)
    {
        _value_min = value;
    }

    public int get_value_max()
    {
        return _value_max;
    }
    public void set_value_max(int value)
    {
        _value_max = value;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        // TODO Auto-generated method stub

        super.onDraw(canvas);
        float width = (float)getWidth();
        float height = (float)getHeight();

        if (width > height)
            _radius = height * (float)0.45;
        else
            _radius = width * (float)0.45;

        _cx = width/2;
        _cy = height/2;

        _angle = map(_value, _value_min, _value_max, _angle_min, _angle_max);

        drawGaugeBG(canvas);
        drawGaugeValue(canvas, true);
    }


    private void drawGaugeBG(Canvas canvas)
    {
        Paint paint = new Paint();
        paint.setColor(_background_color);

        draw_arc_rounded(canvas, _radius, _stroke, _cx, _cy, _angle_min, _angle_max, paint);
    }

    private void drawGaugeValue(Canvas canvas, boolean show_needle)
    {
        if (_value >= 0 && _value <= _value_max) {
            Paint fgPaint = new Paint();
            float[] positions = {0, (float)0.1, map(_value, _value_min, _value_max, (float)0.1, (float) 0.9), 1};
            int[] colors = {_gauge_color_1, _gauge_color_1, _gauge_color_2, _gauge_color_2};

            fgPaint.setShader(new SweepGradient(_cx, _cy, colors, positions));

            Paint bgPaint = new Paint();
            bgPaint.setColor(_background_color);

            draw_arc_rounded(canvas, _radius, _stroke, _cx, _cy, _angle_min, _angle, fgPaint);
            if (show_needle) {
                draw_stroke(canvas, _radius, (int) (_stroke * 0.75), _stroke, _cx, _cy, _angle, fgPaint);
                draw_circle(canvas, _radius, (int) (_stroke * 0.6), _cx, _cy, _angle, bgPaint);
            }
        }
    }


    private void setXmlValues(TypedArray array) {

        _radius = array.getFloat(R.styleable.Gauges_radius, 200);

        _stroke = array.getFloat(R.styleable.Gauges_stroke, 20);
        _background_color = array.getColor(R.styleable.Gauges_background_color, 0);

        _direction = array.getInt(R.styleable.Gauges_direction, 0);

        if (_direction == 0) {
            _angle_min = array.getFloat(R.styleable.Gauges_angle_min, 45);
            _angle_max = array.getFloat(R.styleable.Gauges_angle_max, 315);
            _gauge_color_1 = array.getColor(R.styleable.Gauges_gauge_color_1, 0);
            _gauge_color_2 = array.getColor(R.styleable.Gauges_gauge_color_2, 0);
        }
        else
        {
            _angle_max = array.getFloat(R.styleable.Gauges_angle_min, 45);
            _angle_min = array.getFloat(R.styleable.Gauges_angle_max, 315);
            _gauge_color_2 = array.getColor(R.styleable.Gauges_gauge_color_1, 0);
            _gauge_color_1 = array.getColor(R.styleable.Gauges_gauge_color_2, 0);
        }

        _value_min = array.getInt(R.styleable.Gauges_value_min, 0);
        _value_max = array.getInt(R.styleable.Gauges_value_max, 10000);

        _show_needle = array.getBoolean(R.styleable.Gauges_show_needle, true);
    }
}